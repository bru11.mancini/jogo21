package com.itau;

import java.util.ArrayList;
import java.util.Random;

public class InicializaBaralho {



    public static ArrayList<Cartas> listaCartas = new ArrayList<Cartas>();

    public static void inicializaBaralho(){
        Cartas carta = new Cartas();
        carta.carta = "As";
        carta.naipe = "Ouro";
        carta.valor = 1;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "As";
        carta.naipe = "Espada";
        carta.valor = 1;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "As";
        carta.naipe = "Copas";
        carta.valor = 1;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "As";
        carta.naipe = "Paus";
        carta.valor = 1;
        listaCartas.add((carta));


        carta = new Cartas();
        carta.carta = "Dois";
        carta.naipe = "Ouro";
        carta.valor = 2;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Dois";
        carta.naipe = "Espada";
        carta.valor = 2;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Dois";
        carta.naipe = "Copas";
        carta.valor = 2;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Dois";
        carta.naipe = "Paus";
        carta.valor = 2;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Três";
        carta.naipe = "Ouro";
        carta.valor = 3;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Três";
        carta.naipe = "Espada";
        carta.valor = 3;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Três";
        carta.naipe = "Copas";
        carta.valor = 3;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Três";
        carta.naipe = "Paus";
        carta.valor = 3;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Quatro";
        carta.naipe = "Ouro";
        carta.valor = 4;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Quatro";
        carta.naipe = "Espada";
        carta.valor = 4;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Quatro";
        carta.naipe = "Copas";
        carta.valor = 4;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Quatro";
        carta.naipe = "Paus";
        carta.valor = 4;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Cinco";
        carta.naipe = "Ouro";
        carta.valor = 5;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Cinco";
        carta.naipe = "Espada";
        carta.valor = 5;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Cinco";
        carta.naipe = "Copas";
        carta.valor = 5;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Cinco";
        carta.naipe = "Paus";
        carta.valor = 5;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Seis";
        carta.naipe = "Ouro";
        carta.valor = 6;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Seis";
        carta.naipe = "Espada";
        carta.valor = 6;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Seis";
        carta.naipe = "Copas";
        carta.valor = 6;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Seis";
        carta.naipe = "Paus";
        carta.valor = 6;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Sete";
        carta.naipe = "Ouro";
        carta.valor = 7;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Sete";
        carta.naipe = "Espada";
        carta.valor = 7;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Sete";
        carta.naipe = "Copas";
        carta.valor = 7;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Sete";
        carta.naipe = "Paus";
        carta.valor = 7;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Oito";
        carta.naipe = "Ouro";
        carta.valor = 8;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Oito";
        carta.naipe = "Espada";
        carta.valor = 8;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Oito";
        carta.naipe = "Copas";
        carta.valor = 8;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Oito";
        carta.naipe = "Paus";
        carta.valor = 8;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Nove";
        carta.naipe = "Ouro";
        carta.valor = 9;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Nove";
        carta.naipe = "Espada";
        carta.valor = 9;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Nove";
        carta.naipe = "Copas";
        carta.valor = 9;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Nove";
        carta.naipe = "Paus";
        carta.valor = 9;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Dez";
        carta.naipe = "Ouro";
        carta.valor = 10;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Dez";
        carta.naipe = "Espada";
        carta.valor = 10;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Dez";
        carta.naipe = "Copas";
        carta.valor = 10;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Dez";
        carta.naipe = "Paus";
        carta.valor = 10;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Dama";
        carta.naipe = "Ouro";
        carta.valor = 10;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Dama";
        carta.naipe = "Espada";
        carta.valor = 10;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Dama";
        carta.naipe = "Copas";
        carta.valor = 10;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Dama";
        carta.naipe = "Paus";
        carta.valor = 10;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Valete";
        carta.naipe = "Ouro";
        carta.valor = 10;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Valete";
        carta.naipe = "Espada";
        carta.valor = 10;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Valete";
        carta.naipe = "Copas";
        carta.valor = 10;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Valete";
        carta.naipe = "Paus";
        carta.valor = 10;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Rei";
        carta.naipe = "Ouro";
        carta.valor = 10;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Rei";
        carta.naipe = "Espada";
        carta.valor = 10;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Rei";
        carta.naipe = "Copas";
        carta.valor = 10;
        listaCartas.add((carta));

        carta = new Cartas();
        carta.carta = "Rei";
        carta.naipe = "Paus";
        carta.valor = 10;
        listaCartas.add((carta));



    }

    public int ordenacao(){
        Random r = new Random();
        return r.nextInt(listaCartas.size());
    }


}
