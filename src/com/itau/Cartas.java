package com.itau;

import java.util.ArrayList;

public class Cartas {

     String carta;
     String naipe;
     Integer valor;

    public Cartas() {
    }

    public String getCarta() {
        return carta;
    }

    public String getNaipe() {
        return naipe;
    }

    public Integer getValor() {
        return valor;
    }
}
